import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './pages/signup/signup.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { WORKOUTSComponent } from './pages/workouts/workouts.component';
import { COMMUNITYComponent } from './pages/community/community.component';
import { authGuard } from './auth.guard';

const routes: Routes = [
 
{
  path:'',
  component:LoginComponent,
  pathMatch:'full',
},
{path:"login", component:LoginComponent},
  {
    path:'signup',
    component: SignupComponent,  
    pathMatch: 'full',
  },
  {
    path:'home',
    canActivate:[authGuard],
    component:HomeComponent,
    pathMatch:'full'
  },
  {
    path:'about',
    canActivate:[authGuard],
    component:AboutComponent,
    pathMatch:'full',
  },
  {
    path:'workouts',canActivate:[authGuard],
    
    component:WORKOUTSComponent,
    pathMatch:'full',
  },
  {
    path:'community',
    canActivate:[authGuard],
    component:COMMUNITYComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
