import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl} from '@angular/forms';
import {ThemePalette} from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { FitnessService } from 'src/app/services/fitness.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit{
  fitness:any;
  
  constructor(private service:FitnessService, private snack:MatSnackBar,private router:Router,private fb:FormBuilder){

    this.fitness = { userId: '', username: '', password: '', firstname: '', lastname: '', email: '', phone: '', enabled: 'true',};
   
  }
  ngOnInit() {
    
  }
  
  isPasswordMatch(){

  }

  formSubmit(regForm:any){
    
    this.fitness.username= regForm.username;
    this.fitness.password = regForm.password;
    this.fitness.firstname = regForm.firstname;
    this.fitness.lastname = regForm.lastname;
    this.fitness.email = regForm.email;
    this.fitness.phone=regForm.phone

    console.log(this.fitness);

    if(this.fitness.username == '' || this.fitness.username==null ){
      //alert('User is required');
      this.snack.open('Username is required!! ', '', {
        duration:3000,
        verticalPosition:'top',
        horizontalPosition:'right', 
      } );
    }

  
    this.service.register(this.fitness).subscribe((data: any) => {
      console.log(data);
      Swal.fire('Success','User is registered','success');
      this.router.navigate(['login']);
    });
    
   
  }
}