import { ActivatedRouteSnapshot, CanActivate, CanActivateFn, RouterStateSnapshot, UrlTree } from '@angular/router';
import { FitnessService } from './services/fitness.service';
import { Observable } from 'rxjs';

export class authGuard implements CanActivate {
  constructor(private service:FitnessService){
    
  }
  //return true;
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
   return this.service.getUserLoggedStatus();
};
}
